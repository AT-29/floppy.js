# floppy.js

serve and update static files in node with no dependencies.

cd to example dir and run "node index.js"

>simple example 
```javascript
import * as http from "http";
import floppy from "./floppy.js"; //import module

const server = http.createServer();

server.listen( 3000, () => {
  console.log("open http://localhost:3000" + "/example/index.html");
});

//chain collection
const collection = floppy({
  cmd: {
    list: "storage1/list", //display data route
    write: "storage1/upload", //upload data route
    erase: "storage1/erase", //archive data route
  },
  path: "../data/storage1/", //data storage
  archive: "../data/archive1/", //archive
  staticFiles: { //what files to serve
    ".xml": "text/xml; charset=UTF-8",
    ".js": "text/javascript; charset=UTF-8",
    ".css": "text/css; charset=UTF-8",
    ".json": "text/json; charset=UTF-8",
    ".txt": "text/plain; charset=UTF-8",
    ".html": "text/html; charset=UTF-8",
  },
})
.floppy({
  cmd: {
    list: "storage2/list",
    write: "storage2/upload",
    erase: "storage2/erase",
  },
  path: "../data/storage2/",
  archive: "../data/archive2/",
  staticFiles: {
    ".xml": "text/xml; charset=UTF-8",
    ".json": "text/json; charset=UTF-8",
    ".txt": "text/plain; charset=UTF-8",
  },
})
.floppy({
  cmd: {
    list: "logs/list",
    write: "logs/upload",
    erase: "logs/erase",
  },
  path: "../data/logs",
  archive: "../data/logs",
  staticFiles: {
    ".json": "text/json; charset=UTF-8",
    ".txt": "text/plain; charset=UTF-8",
  },
});

//load collection
server.on("request", collection.load);
```