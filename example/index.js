"use strict";
import { API, PORT } from "./config.js";
import * as http from "http";
import floppy from "../floppy.js";

const server = http.createServer();
server.listen(PORT, () => {
  console.log("listening on port " + PORT);
  console.log("open http://localhost:" + PORT + "/example/index.html");
});

const collection = 
floppy({
  cmd: {
    dir:"storage1/dir",
    list: "storage1/list",
    write: "storage1/upload",
    erase: "storage1/erase",
  },
  path: "../data/storage1/",
  archive: "../data/archive1/",
  staticFiles: {
    ".xml": "text/xml; charset=UTF-8",
    ".js": "text/javascript; charset=UTF-8",
    ".css": "text/css; charset=UTF-8",
    ".json": "text/json; charset=UTF-8",
    ".txt": "text/plain; charset=UTF-8",
    ".html": "text/html; charset=UTF-8",
  },
})
.floppy({
  cmd: {
    dir:"storage2/dir",
    list: "storage2/list",
    write: "storage2/upload",
    erase: "storage2/erase",
  },
  path: "../data/storage2/",
  archive: "../data/archive2/",
  staticFiles: {
    ".xml": "text/xml; charset=UTF-8",
    ".js": "text/javascript; charset=UTF-8",
    ".css": "text/css; charset=UTF-8",
    ".json": "text/json; charset=UTF-8",
    ".txt": "text/plain; charset=UTF-8",
    ".html": "text/html; charset=UTF-8",
  },
});
server.on("request", collection.load);
