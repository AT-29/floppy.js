
import { API, PORT } from "./config.js";

const elements = {
  saveFileButt: document.getElementById("saveFileButt"),
  fileSelector:document.getElementById("fileSelector"),
  dataInput:document.getElementById("dataInput"),
  dirInput:document.getElementById("dirInput"),
  fileContent:document.getElementById("fileContent"),
  selectStorage:document.getElementById("selectStorage"),
  deleteFileButt:document.getElementById("deleteFileButt"),
};

const clearFileOptions = () =>{
  const opt = elements.fileSelector.querySelector("optgroup");
   elements.fileSelector.innerHTML = ''
   elements.fileSelector.appendChild(opt)
 }
 const clearFileContent = () =>{
  elements.fileContent.value='';
  elements.dataInput.value = '';
  elements.dirInput.value = '';
 }
 const loadSelectedFile = (filename) =>{
   if(!filename?.split('.')[1]){
    clearFileContent();clearFileOptions();
    elements.dirInput.value = filename + '/';
    createFileSelect();
   }else{
    const dir = elements.dirInput.value;
    fetch(`${API}${PORT}/data/${elements.selectStorage.value}/${dir}${filename}`)
    .then(res=>res.text())
    .then((res)=>{
      elements.fileContent.value = res;
      elements.dataInput.value = filename;
    })
   }
}
const createFileSelect = () =>{
//  const dir = elements.dirInput.value ? elements.dirInput.value : '';
  fetch(`${API}${PORT}/${elements.selectStorage.value}/list`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body:JSON.stringify({dir:elements.dirInput.value}),
  }).then(res=>res.json()).then((data)=>{
    data.items.map((file)=>{
    const opt = document.createElement('option');
    opt.className = 'options';
    opt.value = file
    opt.innerHTML = file
    elements.fileSelector.appendChild(opt)
    })
  });
}

const saveFile = () => {
  const dir = elements.dirInput.value;
  const save = (dir) =>{
    return fetch(`${API}${PORT}/${elements.selectStorage.value}/upload`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body:JSON.stringify({filename: elements.dataInput.value,data: elements.fileContent.value, dir}),
    });
  }

  if(dir.split('/').length>1){
    fetch(`${API}${PORT}/${elements.selectStorage.value}/dir`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body:JSON.stringify({dir}),
    }).then(()=>save('/'+dir)).then(()=>{
      clearFileOptions();
      createFileSelect()
      // const newDir = document.createElement('option');
      // newDir.value =  '/'+ dir;
      // elements.textContent = '/'+ dir;
      // elements.selectStorage.appendChild(newDir);
      // clearFileOptions();createFileSelect()
    })
  }else{
    save('').then(()=>{clearFileOptions();createFileSelect()});
  }

}; 

const deleteFile = () => {
  const dir = elements.dirInput.value;
  fetch(`${API}${PORT}/${elements.selectStorage.value}/erase`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body:JSON.stringify({filename: elements.dataInput.value,data: elements.fileContent.value,dir}),
  }).then(()=>{clearFileContent();clearFileOptions();createFileSelect()});
};  

const emptyFolder = () =>{
  fetch(`${API}${PORT}/${this.elements.selectStorage.value}/empty`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },body:JSON.stringify({dir:"/"})
})
}
  elements.saveFileButt.addEventListener("click", () => saveFile());
  elements.deleteFileButt.addEventListener("click", () => deleteFile());
  elements.selectStorage.addEventListener("change", () => {clearFileContent();clearFileOptions();createFileSelect();});
  elements.fileSelector.addEventListener("click", (e) =>{ loadSelectedFile(e.target.value); } );
  document.addEventListener('DOMContentLoaded',()=>{clearFileOptions();createFileSelect();})
