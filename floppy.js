import * as url from "url";
import * as fs from "fs";
import { fileURLToPath } from "url";
import { dirname } from "path";

const __dirname = dirname(fileURLToPath(import.meta.url));

const floppy = (options) => {
  const request = new Map();

  const load = (req, res) => {
    const method = req.method;
    const urlParsed = url.parse(req.url);

    const query = urlParsed.query;

    if (req.url === "/favicon.ico") {
      res.writeHead(200, { "Content-Type": "image/x-icon" });
      res.end();
      return;
    }

    let body = "";
    req.on("data", (chunk) => {
      body += chunk;
    });

    req.on("end", () => {
      const callback = request.get(`${method}:${urlParsed.pathname}`);
      if (method === "POST") {
        if (query && body) {
          req.query = query;
          req.body = body;
        } else if (body) {
          req.body = body;
        }
      }

      if (callback) {
        callback.call({}, req, res);
      } else if (method !== "POST") {
        if (options.staticFiles) {
          res.writeHead(404, {
            "Content-Type": "application:json",
          });

          const purl = url.parse(req.url, true);

          fs.access(__dirname + purl.pathname, (err) => {
            const filePath = purl.pathname.split("/").pop();
            const fileName = filePath.split(".");
            const extention = "." + fileName.pop();
            const file = fileName.join(".");
            if (file) {
              if (err) {
                res.writeHead(404, { "Content-Type": "text/plain" });
                res.end();
              }
              if (!options.staticFiles[extention]) {
                console.error("Static File type is not provided.");
                res.writeHead(200, { "Content-Type": "application/json" });
                res.end();
              } else {
                if (body.length > 1e6) req.socket.destroy();
                res.writeHead(200, {
                  "Content-Type": options.staticFiles[extention],
                });
                fs.readFile(__dirname + purl.pathname, (err, data) => {
                  try {
                    res.end(data);
                  } catch (err) {
                    console.log(err);
                    res.end();
                  }
                });
              }
            }
          });
        }
      }
    });
  };

  const floppy = (options) => {
    if (options.cmd.list) {
      request.set("POST:/" + options.cmd.list, (req, res) => {
        res.writeHead(200, { "Content-Type": "application/json" });
        const json = JSON.parse(req.body);
        const dir = json.dir ? "/" + json.dir + "/" : "";
        fs.readdir(options.path + dir, (err, files) => {
          let items = [];
          if (files) {
            files.forEach((file) => {
              items.push(file);
            });
          }
          if (err) {
            console.error(err);
            res.end();
          }
          res.end(JSON.stringify({ items }));
        });
      });
    }
    if (options.cmd.dir) {
      request.set("POST:/" + options.cmd.dir, (req, res) => {
        res.writeHead(200, { "Content-Type": "application/json" });
        const json = JSON.parse(req.body);
        fs.mkdir(`${options.path}/${json.dir}`, { recursive: true }, (err) => {
          if (err) {
            console.error(err);
            res.end();
          } else {
            res.end(json.dir + " directory created");
          }
        });
      });
    }
    if (options.cmd.write) {
      request.set("POST:/" + options.cmd.write, (req, res) => {
        res.writeHead(200, { "Content-Type": "application/json" });
        const json = JSON.parse(req.body);
        const dir = json.dir ? json.dir + "/" : "";
        fs.writeFile(
          `${options.path}${dir}${json.filename}`,
          json.data,
          (err) => {
            if (err) {
              console.log(err);
              res.end();
            }
            res.end();
          }
        );
      });
    }
    if (options.cmd.erase) {
      request.set("POST:/" + options.cmd.erase, (req, res) => {
        res.writeHead(200, { "Content-Type": "application/json" });
        const json = JSON.parse(req.body);
        const dir = json.dir ? json.dir + "/" : "";
        fs.writeFile(
          `${options.archive}${json.newfilename}`,
          json.data,
          (err) => {
            fs.unlink(`${options.path}${dir}${json.filename}`, (err) => {
              if (err) throw err;
              res.end();
            });
            if (err) throw err;
            res.end();
          }
        );
      });
    }
    if (options.cmd.empty) {
      request.set("POST:/" + options.cmd.empty, (req, res) => {
        res.writeHead(200, { "Content-Type": "application/json" });
        const json = JSON.parse(req.body);
        const dir = json.dir ? json.dir + "/" : "";
        fs.readdir(options.path + dir, (err, files) => {
          if (err) throw err;
          if (files.length > 0)
            fs.rmdir(options.path + dir, { recursive: true }, () => {});
          res.end();
        });
      });
    }
    return { floppy, load };
  };

  floppy(options);
  return { floppy, load };
};
export default floppy;
